terraform {
  backend "remote" {
    organization = "terraform-exam-02"

    workspaces {
      name = "exam-prep"
    }
  }
}

variable "var1" {}
variable "var2" {}

resource "random_pet" "p1" {
  prefix = var.var2
}


resource "random_pet" "p2" {
  prefix = var.var1
}
